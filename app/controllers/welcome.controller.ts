import { Request, Response } from 'express';

/**
 * A controller action for our home page, just with plain text
 * @param res 
 * @param resp 
 */
export let index = (req:Request, resp:Response) => {
    resp.send("Hello world, it's express here!")
}

export let receiveJsonPost = (req:Request, resp:Response) => {

    let obj = req.body

    console.log(req.body)
    resp.send("Name was " + obj["name"])
}

export let respondWithJson = (req:Request, resp:Response) => {
    resp.send({
        "Hello": ["World", "Alice", "Bob"],
        "Goodbye": {
            "Mr": "Chips"
        }
    })
}

const chars = "abcdefghijklmnopqrstuvwxyz0123456789"

export let game = (req:Request, resp:Response) => {

    let id = req.query.id

    let randomId = () => {
        let code = ""
        for (let i = 0; i < 8; i++) {
            let n = Math.floor(Math.random() * chars.length)
            code += chars.charAt(n)
        }
        return code
    }

    if (id !== undefined) {
        resp.send("The id was " + id)
    } else {
        resp.redirect("/example/game?id=" + randomId())
    }
}