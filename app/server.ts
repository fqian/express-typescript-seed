import express from 'express';
import http from 'http';
import SocketIO from 'socket.io';
import bodyParser from "body-parser";
import * as path from "path";

import * as welcome from "./controllers/welcome.controller"

const app = express()
const httpApp = new http.Server(app)
const io = SocketIO(httpApp)

// This enables routes expection JSON data to access it as req.body
app.use(bodyParser.json())

// This maps the /build/public directory to the /public path
app.use("/public", express.static(path.join(__dirname, "public")))

// Which port to use
const port: string = process.env.PORT || "3000"

/*
 * Routes
 */
app.get('/', welcome.index)
app.get("/example/json", welcome.respondWithJson)
app.post("/example/json", welcome.receiveJsonPost)
app.get("/example/game", welcome.game)

io.on('connection', (socket) => {
  console.log("A user connected")

  socket.on('disconnect', (evt) => {
    console.log("Disconnected")
  })
})

/*
 * Start the server
 */ 
const server = httpApp.listen(port, () => {
  console.log("App running on port %d", port);    
});

export default httpApp


