# express-ts-seed

This is a seed project with a simple set-up for express using TypeScript.

The `/build/public` path is exposed on the `/public` route to allow serving static files. Note, though, that `/build/` is set to be ignored by git. This lets you, for instance, place your client-side code built with webpack into `/build/public`.

A body-parser for JSON data is installed by default. The app has also been given a directory structure a little like MVC frameworks. For example, controllers in `/app/controllers`. Routes are wired to controller actions in `server.ts`

To install dependencies:

```sh
npm install
```

To compile the code:

```sc
npm run tsc
```

To run the server:

```sh
node build/server.js
```